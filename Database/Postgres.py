from datetime import datetime
import json
from decimal import Decimal
import os
import psycopg2


class Postgres:
    def __init__(self,
    host: str = "dasdb.cnmplr74tcrx.eu-west-2.rds.amazonaws.com",
    dbname: str = "das",
    password: str = "wIe8kaes",
    port: int = 5432,
    username: str = "postgres"
    ):
        self.host = host
        self.dbname = dbname
        self.password = password
        self.port = port
        self.username = username

    def get(self, sql):
        result = 0
        try:
            conn = psycopg2.connect(
                host=self.host,
                port=self.port,
                dbname=self.dbname,
                user=self.username,
                password=self.password
            )

            cur = conn.cursor()
            sql = sql + ';'
            cur.execute(sql)
            result = cur.fetchall()
        finally:
            try:
                conn.close()
            except:
                pass
        return result

    def put(self, sql):
        result = 0
        try:
            conn = psycopg2.connect(
                host=self.host,
                port=self.port,
                dbname=self.dbname,
                user=self.username,
                password=self.password
            )

            cur = conn.cursor()
            cur.execute(sql + ';')
            conn.commit()
        finally:
            try:
                cur.close()
                conn.close()
            except:
                pass
