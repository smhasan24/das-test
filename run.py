from flask import Flask, render_template, request, redirect, url_for, session, g
import re
import requests
from DeviceClass.ConditionMonitoring import ConditionMonitoring
from DeviceClass.AssestTracking import AssestTracking

app = Flask(__name__)

app.secret_key = 'BAD_SECRET_KEY'

# ------------------ Containers ------------------------------------
@app.route("/container-map.html")
def container_map():
    return render_template('container-map.html')

@app.route("/container-menu.html")
def container_menu():
    id = request.form.get('id')
    return render_template('container-menu.html', query=id)

@app.route("/container-condition-monitoring.html")
def container_condition_monitoring():
    id = request.form.get('id')
    return render_template('container-condition-monitoring.html', query=id)

@app.route("/container-alarms.html")
def container_alarms():
    id = request.form.get('id')
    return render_template('container-alarms.html', query=id)

@app.route("/container-access-history.html")
def container_access_history():
    id = request.form.get('id')
    return render_template('container-access-history.html', query=id)

@app.route("/container-stock-management.html")
def container_stock_management():
    id = request.form.get('id')
    return render_template('container-stock-management.html', query=id)

@app.route("/container-thresholds-settings.html")
def container_thresholds_settings():
    id = request.form.get('id')
    return render_template('container-thresholds-settings.html', query=id)

@app.route("/container-orders.html")
def container_orders():
    id = request.form.get('id')
    return render_template('container-orders.html', query=id)
# ------------------ Items ------------------------------------
@app.route("/items.html")
def items_menu():
    return render_template('items.html')

@app.route("/item-type-search.html")
def item_type_search():
    return render_template('item-type-search.html')

@app.route("/container-search.html")
def container_search():
    return render_template('container-search.html')

@app.route("/item-management.html")
def item_management():
    return render_template('item-management.html')
# ------------------ Admin ------------------------------------
@app.route("/admin.html")
def admin():
    return render_template('admin.html')

@app.route("/user-management.html")
def user_management():
    return render_template('user-management.html')

@app.route("/container-management.html")
def container_management():
    return render_template('container-management.html')

@app.route("/item-type-management.html")
def item_type_management():
    return render_template('item-type-management.html')

# ------------------ Authentication ------------------------------------
@app.route("/")
def index():
    # Check if cookie exists
    app.logger.info('homeroute')
    logged_in = 'user' in session
    if logged_in:
        return render_template('container-map.html')
    # Check if card has been swipped
    else:
        return render_template('auth.html')

    return render_template('container-map.html')

@app.route("/auth.html")
def auth():
    return render_template('auth.html')

@app.route("/log-out.html")
def logout():
    # delete cookie
    session.pop('user', default=None)
    return redirect(url_for('index'))

@app.route('/api/login', methods=['POST'])
def app_login():
    if request.method == 'POST':
        request_data = request.get_json()
        app.logger.info(request_data)
        email = request_data['username']
        passw = request_data['password']
        url = 'https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/login'
        obj = {
            'username':email,
            'password': passw
        }
        r = requests.post(url, json=obj)
        app.logger.info(r.text)
        if r.status_code == 200:
            app.logger.info(r.json())
            session['user'] = r.json()
            # app.logger.info(session['user'])
            return '200'
        if r.status_code == 403:
            return '403'
        if r.status_code == 404:
            return '404'



@app.route("/get-session")
def get_session():
    return session['user']

@app.route("/health-check")
def health_check():
    return '200'

@app.before_request
def load_user():
    # does session exist
    path = request.path

    logged_in = 'user' in session

    if logged_in:
        print('do what ever user wants')
    if path in ['/auth.html', '/', '/api/login, /health-check']:
        app.logger.info('no need to redirect')
    elif 'user' not in session and not re.match('/static/', request.path) and not re.match('/api/', request.path):
        app.logger.info(path)
        return redirect(url_for('auth.html'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=80)
