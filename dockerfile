FROM continuumio/miniconda3

WORKDIR /app

# Create the environment:
COPY environment.yml .
RUN conda env create -f environment.yml

# Make RUN commands use the new environment:
RUN echo "conda activate flask_server" >> ~/.bashrc
SHELL ["/bin/bash", "--login", "-c"]

# Demonstrate the environment is activated:
RUN echo "Make sure flask is installed:"
RUN python -c "import flask"

# The code to run when container is started:
COPY run.py ep.sh ./
COPY templates templates
COPY static static
COPY DeviceClass DeviceClass
COPY Database Database

RUN ["chmod", "+x", "ep.sh"]
EXPOSE 80
ENTRYPOINT ["./ep.sh"]
