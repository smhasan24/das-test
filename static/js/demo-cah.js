const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/container/access_history?id=' + id;
var header = document.getElementById("header-cah");
header.innerText = "Container " + id + "  Access History";

function menu(page) {
  if (page == 'containers') {
    location.href = "demo.html"
  }
  else if (page == 'container menu') {
    location.href = "demo-selected-container.html?id=" + id;
  }
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

Http.open("GET", url);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);

    console.log(response);

    let table = document.getElementById("table-access-history");
    let data = Object.keys(response[0]);
    generateTableHead(table, data);
    generateTable(table, response);
  }

}
