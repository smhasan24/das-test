const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/containers';
const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];

var header = document.getElementById("header-menu");
header.innerText = "Container " + id + "  Menu";

function menu(page) {
  if (page == 'containers') {
    location.href = "demo.html"
  }
  else if (page == 'container menu') {
    location.href = "demo-selected-container.html?id=" + id;
  }
}

document.getElementById("ccm").onclick = function () {
        location.href = "demo-ccm.html?id=" + id;
    };
document.getElementById("ci").onclick = function () {
        location.href = "demo-ci.html?id=" + id;
    };
document.getElementById("cah").onclick = function () {
        location.href = "demo-cah.html?id=" + id;
    };
document.getElementById("ca").onclick = function () {
        location.href = "demo-ca.html?id=" + id;
    };
document.getElementById("ct").onclick = function () {
        location.href = "demo-ct.html?id=" + id;
    };
