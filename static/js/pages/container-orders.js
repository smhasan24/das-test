const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/container/orders';
var qs = "?id="+id;
var qsat = "";
var qsas = "";
var qsts = "";
var initFlag = true;

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "container-menu.html?id=" + id;
    };

function clearTable(table){
  $("#table-body").remove();
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    let row = tbody.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}
function tableInit(){
  $('#table-alarms').DataTable({
      "language": {
          "paginate": {
              "previous": "<i class='mdi mdi-chevron-left'>",
              "next": "<i class='mdi mdi-chevron-right'>"
          }
      },
      "drawCallback": function () {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
      },
  });
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs + qsat + qsas + qsts);
  Http.send();
}
Http.open("GET", url + qs);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        //tableInit();
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}

getRequest()
!function($) {
    "use strict";
    var FormAdvanced = function() {};
    //initializing tooltip
    FormAdvanced.prototype.initSelect2 = function() {
        // Select2
        $('#alarm-type').select2();
        $('#alarm-state').select2();
    },
    //initilizing
    FormAdvanced.prototype.init = function() {
        var $this = this;
        this.initSelect2()
        $('.input-daterange-timepicker').daterangepicker({
          timePicker: true,
          locale: {format: 'DD/MM/YYYY hh:mm A'},
          timePickerIncrement: 30,
          timePicker12Hour: true,
          timePickerSeconds: false,
          buttonClasses: ['btn', 'btn-sm'],
          applyClass: 'btn-secondary',
          cancelClass: 'btn-primary',
          startDate: moment().subtract(1, 'days'),
          endDate: moment()
        },function (start, end) {
            console.log(start.toISOString(), end.toISOString());
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            qsts = "&t1=" + start.toISOString() + "&t2=" + end.toISOString()
            getRequest()

        });
    },
    $.FormAdvanced = new FormAdvanced, $.FormAdvanced.Constructor = FormAdvanced

}(window.jQuery),
    //initializing main application module
    function ($) {
        "use strict";
        $.FormAdvanced.init();
    }(window.jQuery);
