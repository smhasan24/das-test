const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/container/items';
var qs = "?all="+id;
var qsat = "";
var qsas = "";
var qsts = "";
var initFlag = true;


const Http_item_type = new XMLHttpRequest();
var type_url = "https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/items/types"
Http_item_type.open("GET", type_url);
Http_item_type.send();


Http_item_type.onreadystatechange = (e) => {
  if(Http_item_type.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http_item_type.responseText);
    console.log(response)
    for (itemType in response){
      var optgroup = document.getElementById("item-type-og");
      var option = document.createElement("option");
      option.innerHTML = response[itemType]["type"];
      optgroup.appendChild(option);
    }
  }
}

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "items.html";
    };

function clearTable(table){
  $("#table-body").remove();
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    let row = tbody.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs + qsat + qsas + qsts);
  Http.send();
}
Http.open("GET", url + qs);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
      }
    } else {
      clearTable();
    }

  }
}

$('#item-type').on('change', function (e) {
  var selectData = $('#item-type').select2('data');
  // for selected data run a new get
  if (selectData.length > 0) {
    dataString = '[';
    for (data in selectData){
      dataString = dataString + '"' + selectData[data]['text'] + '",'
    }
    dataString = dataString.slice(0, -1);
    dataString = dataString + ']'
    console.log(dataString);
    qsat = '&it=' + dataString
  } else {
    qsat = ""
  }
  console.log(qsat);
  getRequest()
});

getRequest()
!function($) {
    "use strict";
    var FormAdvanced = function() {};
    //initializing tooltip
    FormAdvanced.prototype.initSelect2 = function() {
        // Select2
        $('#item-type').select2();
    },
    //initilizing
    FormAdvanced.prototype.init = function() {
        var $this = this;
        this.initSelect2()

    },
    $.FormAdvanced = new FormAdvanced, $.FormAdvanced.Constructor = FormAdvanced

}(window.jQuery),
    //initializing main application module
    function ($) {
        "use strict";
        $.FormAdvanced.init();
    }(window.jQuery);
