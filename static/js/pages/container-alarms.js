const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];
const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/container/alarms';
var qs = "?id="+id;
var qsat = "";
var qsas = "";
var qsts = "";
var initFlag = true;

var tableIDs = [];

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "container-menu.html?id=" + id;
    };

function clearTable(table){
  $("#table-body").remove();
}
document.getElementById("deactivate").onclick = function () {
        //get all ids of alarms
        $.ajax({
          url: "/get-session",
          type: 'GET',
          success: function(res) {
              console.log(res);
              var alarms = tableIDs;
              var user = res['id'];
              var timestamp = new Date().toISOString();
              var setAlarm = false;
              var data = {
                alarms: alarms,
                user: user,
                ts: timestamp,
                setAlarm: setAlarm
              }
              $.ajax({
                type: "POST",
                url: url + "/update/all",
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                success: function(res) {
                  getRequest();
                }
              });
          }
        });
    };
function clearAlarmByID(id){
  // get session from Flask
  $.ajax({
    url: "/get-session",
    type: 'GET',
    success: function(res) {
        console.log(res);
        var alarm = id
        var user = res['id']
        var timestamp = new Date().toISOString()
        var setAlarm = false;
        var us = "?setAlarm="+setAlarm+"&ts="+timestamp+"&user="+user+"&alarm="+alarm
        $.ajax({
          url: url + "/update" + us,
          type: 'GET',
          success: function(res) {
              getRequest();
          }
        });
    }
  });
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    if (key == 'Id'){
      continue;
    }
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}
function generateTable(table, data) {
  let tbody = table.createTBody();
  tbody.setAttribute("id", "table-body");
  for (let element of data) {
    tableIDs.push(element['Id']);
    let row = tbody.insertRow();

    for (key in element) {
      if (key == "Id"){
        continue;
      }
      if (key == 'Status'){
        let cell = row.insertCell();
        let clearButton = document.createElement("div");
        clearButton.innerHTML = element[key]
        if (element[key] == 'Active')
        {
            clearButton.className = "bg-success rounded-pill text-white text-center"
            clearButton.addEventListener("click", function() {
            clearAlarmByID(element["Id"])
          });
        } else {
          clearButton.className = "bg-danger rounded-pill text-white text-center"
        }
        cell.appendChild(clearButton);
        continue;
      }
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
    // let cell = row.insertCell();
    // let clearButton = document.createElement("button");
    // clearButton.innerHTML = "del"
    // cell.appendChild(clearButton);
    // clearButton.addEventListener("click", function() {
    //   clearAlarmByID(element["Description"])
    // });
  }
}
function tableInit(destroy){
  $('#table-alarms').DataTable({
      "destroy": destroy,
      "stateSave": false,
      "order": [],
      "language": {
          "paginate": {
              "previous": "<i class='mdi mdi-chevron-left'>",
              "next": "<i class='mdi mdi-chevron-right'>"
          }
      },
      "drawCallback": function () {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
      },
  });
}

function getRequest(){
  console.log('send a request');
  Http.open("GET", url + qs + qsat + qsas + qsts);
  Http.send();
}

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    console.log(response);

    if (response.length > 0){
      let table = document.getElementById("table-alarms");
      let data = Object.keys(response[0]);
      if (initFlag){

        generateTableHead(table, data);
        generateTable(table, response);
        tableInit(false);
        initFlag = false;
      } else {
        //clear table
        clearTable(table);
        generateTable(table, response);
        tableInit(true)
      }
    } else {
      clearTable();
    }

  }
}

$('#alarm-type').on('change', function (e) {
  var selectData = $('#alarm-type').select2('data');
  // for selected data run a new get
  if (selectData.length > 0) {
    dataString = "(";
    for (data in selectData){
      dataString = dataString + "'" + selectData[data]['text'] + "',"
    }
    dataString = dataString.slice(0, -1);
    dataString = dataString + ")"
    console.log(dataString);
    qsat = '&at=' + dataString
  } else {
    qsat = ""
  }
  getRequest()
});
$('#alarm-state').on('change', function (e) {
  var selectData = $('#alarm-state').select2('data');
  if (selectData.length > 0) {
    dataString = "(";
    for (data in selectData){
      dataString = dataString + "" + selectData[data]['id'] + ","
    }
    dataString = dataString.slice(0, -1);
    dataString = dataString + ")"
    console.log(dataString);
    qsas = '&as=' + dataString
  } else {
    qsas = ""
  }
  getRequest()

});

getRequest()
!function($) {
    "use strict";
    var FormAdvanced = function() {};
    //initializing tooltip
    FormAdvanced.prototype.initSelect2 = function() {
        // Select2
        $('#alarm-type').select2();
        $('#alarm-state').select2();
    },
    //initilizing
    FormAdvanced.prototype.init = function() {
        var $this = this;
        this.initSelect2()
        $('.input-daterange-timepicker').daterangepicker({
          timePicker: true,
          locale: {format: 'DD/MM/YYYY hh:mm A'},
          timePickerIncrement: 30,
          timePicker12Hour: true,
          timePickerSeconds: false,
          buttonClasses: ['btn', 'btn-sm'],
          applyClass: 'btn-secondary',
          cancelClass: 'btn-primary',
          startDate: moment().subtract(1, 'days'),
          endDate: moment()
        },function (start, end) {
            console.log(start.toISOString(), end.toISOString());
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            qsts = "&t1=" + start.toISOString() + "&t2=" + end.toISOString()
            // Http.open("GET", url+qs);
            // Http.send();
            getRequest()

        });
    },
    $.FormAdvanced = new FormAdvanced, $.FormAdvanced.Constructor = FormAdvanced

}(window.jQuery),
    //initializing main application module
    function ($) {
        "use strict";
        $.FormAdvanced.init();
    }(window.jQuery);
