const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/containers';

function generateMarkers(data){
  let markers = []
  for (row in data){
    var id = data[row]['Id']
    var lat = data[row]['Lattitude']
    var lng = data[row]['Longitude']
    var name = data[row]['Description']
    var object = {
      latLng: [lat,lng],
      name: name,
      id: id
    }
    markers.push(object);
  }
  console.log(markers);
  $('#container-map-markers').vectorMap({
      map: 'world_mill_en',
      normalizeFunction: 'polynomial',
      hoverOpacity: 0.7,
      hoverColor: false,
      zoomMax: 15,
      regionStyle: {
          initial: {
              fill: '#d4dadd'
          }
      },
      markerStyle: {
          initial: {
              r: 9,
              'fill': '#1abc9c',
              'fill-opacity': 0.9,
              'stroke': '#fff',
              'stroke-width': 7,
              'stroke-opacity': 0.4
          },

          hover: {
              'stroke': '#fff',
              'fill-opacity': 1,
              'stroke-width': 1.5
          }
      },
      backgroundColor: 'transparent',
      markers: markers,
      onMarkerClick: function(event, index) {
                      // alter the weburl
                      location.href = "container-menu.html?id=" + markers[index].id;
                  }
  });
}
function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

function addRowHandlers(table) {
  var rows = table.getElementsByTagName("tr");
  for (i = 0; i < rows.length; i++) {
    var currentRow = table.rows[i];
    var createClickHandler = function(row) {
      return function() {
        var cell = row.getElementsByTagName("td")[0];
        var id = cell.innerHTML;
        //alert("id:" + id);
        location.href = "container-menu.html?id=" + id;
      };
    };
    currentRow.onclick = createClickHandler(currentRow);
  }
}

Http.open("GET", url);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);

    let table = document.getElementById("table-containers");
    let data = Object.keys(response[0]);
    generateTableHead(table, data);
    generateTable(table, response);
    generateMarkers(response);
    addRowHandlers(table);
  }
}
