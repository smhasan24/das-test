const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());
var id = params['id'];

const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/condition-monitoring';
var end = moment()
var start = moment().subtract(5,'days')


const options = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  second:'numeric',
  minute:'numeric',
  hour:'numeric'
};
console.log(end);
var qs = "?container_id=" + id + "&t1='" + start.toISOString() + "'&t2='" + end.toISOString() + "'"
//endpoint to get container history between
Http.open("GET", url + "?container_id=" + id);
Http.send();

document.getElementById("container-menu-bc").onclick = function () {
        location.href = "container-menu.html?id=" + id;
    };

//update graph data

var timeAxis = [];
var temperature = [];
var humidity = []
// var shock = []
var light = []

function setGraphData(data) {
  timeAxis.splice(0, timeAxis.length)
  temperature.splice(0, temperature.length)
  humidity.splice(0, humidity.length)
  // shock.splice(0, shock.length)
  light.splice(0, light.length)
  for (row in data) {
    timeAxis.push(data[row]['Timestamp'])
    temperature.push(data[row]['Temperature (C)'])
    humidity.push(data[row]['Humidity (%)'])
    // shock.push(data[row]['Shock (%)'])
    light.push(((data[row]['Light'] == "light") ? 1 : 0))
  }
  console.log(temperature);

}

function setWidgetData(response){
  let tempWidget = document.getElementById("temperature-widget");
  let humidWidget = document.getElementById("humidity-widget");
  // let shockWidget = document.getElementById("shock-widget");
  let lightWidget = document.getElementById("light-widget");
  var t = response['Temperature (C)']
  var h = response['Humidity (%)']
  var s = response['Shock (%)']
  var l = response['Light']
  tempWidget.innerHTML = t;
  humidWidget.innerHTML = h;
  // shockWidget.innerHTML = s;
  lightWidget.innerHTML = l;

}

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);
    if (response.length > 0){
      console.log('There is some data');
      if (response[0]["Latest"]) {
        console.log('There is the latest data');
        setWidgetData(response[0]);
        Http.open("GET", url + qs);
        Http.send();
      }
      else {
        console.log('There is data for the graphs');
        document.getElementById("graphs").style.display = "block";
        document.getElementById("no-data").style.display = "none";
        setGraphData(response);


      }
      $.ChartJs.init()
    }
    else{
      console.log('There seems to be no data from the response');
      setGraphData(response);
      document.getElementById("graphs").style.display = "none";
      document.getElementById("no-data").style.display = "block";
    }

  }
}

function hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }
}

! function ($) {
    "use strict";

    var ChartJs = function () {
        this.$body = $("body"),
            this.charts = []
    };

    ChartJs.prototype.respChart = function (selector, type, data, options) {

        // get selector by context
        var ctx = selector.get(0).getContext("2d");
        // pointing parent container to make chart js inherit its width
        var container = $(selector).parent();

        //default config
        Chart.defaults.global.defaultFontColor = "#8391a2";
        Chart.defaults.scale.gridLines.color = "rgba(150, 150, 150, 0.1)";

        // this function produce the responsive Chart JS
        function generateChart() {
            // make chart width fit with its container
            var ww = selector.attr('width', $(container).width());
            return new Chart(ctx, { type: 'line', data: data, options: options });;
        };
        // run function - render chart at first load
        return generateChart();
    },
        // init various charts and returns
        ChartJs.prototype.initCharts = function () {
            var charts = [];
            var defaultColors = ["#1abc9c", "#f1556c", "#3bafda", "#e3eaef"];
            var chartName = '#line-chart-temperature'
            if ($(chartName).length > 0) {
                var dataColors = $(chartName).data('colors');
                var colors = dataColors? dataColors.split(",") : defaultColors.concat();

                var lineChart = {
                    labels: timeAxis,
                    datasets: [{
                        label: "Temperature",
                        backgroundColor: hexToRGB(colors[0], 0.3),
                        borderColor: colors[0],
                        data: temperature
                    }]
                };

                var lineOpts = {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        intersect: false
                    },
                    hover: {
                        intersect: true
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            reverse: true,
                            gridLines: {
                                color: "rgba(150, 150, 150, 0.1)",
                                zeroLineColor: 'rgba(150, 150, 150, 0.1)',
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 20
                            },
                            display: true,
                            borderDash: [5, 5],

                            gridLines: {
                                color: "rgba(0,0,0,0)",

                                fontColor: '#fff'
                            }
                        }]
                    }
                };
                charts.push(this.respChart($(chartName), 'Line', lineChart, lineOpts));
            }
            var chartName = '#line-chart-humidity'
            if ($(chartName).length > 0) {
                var dataColors = $(chartName).data('colors');
                var colors = dataColors? dataColors.split(",") : defaultColors.concat();

                var lineChart = {
                    labels: timeAxis,
                    datasets: [{
                        label: "Humidity",
                        backgroundColor: hexToRGB(colors[0], 0.3),
                        borderColor: colors[0],
                        data: humidity
                    }]
                };

                var lineOpts = {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        intersect: false
                    },
                    hover: {
                        intersect: true
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            reverse: true,
                            gridLines: {
                                color: "rgba(150, 150, 150, 0.1)",
                                zeroLineColor: 'rgba(150, 150, 150, 0.1)',
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 20
                            },
                            display: true,
                            borderDash: [5, 5],

                            gridLines: {
                                color: "rgba(0,0,0,0)",

                                fontColor: '#fff'
                            }
                        }]
                    }
                };
                charts.push(this.respChart($(chartName), 'Line', lineChart, lineOpts));
            }
            // var chartName = '#line-chart-shock'
            // if ($(chartName).length > 0) {
            //     var dataColors = $(chartName).data('colors');
            //     var colors = dataColors? dataColors.split(",") : defaultColors.concat();
            //
            //     var lineChart = {
            //         labels: timeAxis,
            //         datasets: [{
            //             label: "Shock",
            //             backgroundColor: hexToRGB(colors[0], 0.3),
            //             borderColor: colors[0],
            //             data: shock
            //         }]
            //     };
            //
            //     var lineOpts = {
            //         maintainAspectRatio: false,
            //         legend: {
            //             display: false
            //         },
            //         tooltips: {
            //             intersect: false
            //         },
            //         hover: {
            //             intersect: true
            //         },
            //         plugins: {
            //             filler: {
            //                 propagate: false
            //             }
            //         },
            //         scales: {
            //             xAxes: [{
            //                 reverse: true,
            //                 gridLines: {
            //                     color: "rgba(150, 150, 150, 0.1)",
            //                     zeroLineColor: 'rgba(150, 150, 150, 0.1)',
            //                 }
            //             }],
            //             yAxes: [{
            //                 ticks: {
            //                     stepSize: 20
            //                 },
            //                 display: true,
            //                 borderDash: [5, 5],
            //
            //                 gridLines: {
            //                     color: "rgba(0,0,0,0)",
            //
            //                     fontColor: '#fff'
            //                 }
            //             }]
            //         }
            //     };
            //     charts.push(this.respChart($(chartName), 'Line', lineChart, lineOpts));
            // }
            var chartName = '#line-chart-light'
            if ($(chartName).length > 0) {
                var dataColors = $(chartName).data('colors');
                var colors = dataColors? dataColors.split(",") : defaultColors.concat();

                var lineChart = {
                    labels: timeAxis,
                    datasets: [{
                        label: "Light",
                        backgroundColor: hexToRGB(colors[0], 0.3),
                        borderColor: colors[0],
                        data: light
                    }]
                };

                var lineOpts = {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        intersect: false
                    },
                    hover: {
                        intersect: true
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        }
                    },
                    scales: {
                        xAxes: [{
                            reverse: true,
                            gridLines: {
                                color: "rgba(150, 150, 150, 0.1)",
                                zeroLineColor: 'rgba(150, 150, 150, 0.1)',
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 20
                            },
                            display: true,
                            borderDash: [5, 5],

                            gridLines: {
                                color: "rgba(0,0,0,0)",

                                fontColor: '#fff'
                            }
                        }]
                    }
                };
                charts.push(this.respChart($(chartName), 'Line', lineChart, lineOpts));
            }

            return charts;
        },
        //initializing various components and plugins
        ChartJs.prototype.init = function () {
            var $this = this;
            // font
            Chart.defaults.global.defaultFontFamily = 'Nunito,sans-serif';

            // init charts
            $this.charts = this.initCharts();

            // enable resizing matter
            $(window).on('resize', function (e) {
                $.each($this.charts, function (index, chart) {
                    try {
                        chart.destroy();
                    }
                    catch (err) {
                    }
                });
                $this.charts = $this.initCharts();
            });
        },

        //init ChartJs
        $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
}(window.jQuery),

//initializing ChartJs


/* utility function */

!function ($) {
    "use strict";

    var FormPickers = function () { };

    FormPickers.prototype.init = function () {

        $('.input-daterange-timepicker').daterangepicker({
            beforeShow: function (textbox, instance) {
                 var txtBoxOffset = $(this).offset();
                 var top = txtBoxOffset.top;
                 var left = txtBoxOffset.left;
                 var textBoxWidth = $(this).outerWidth();
                 console.log('top: ' + top + 'left: ' + left);
                         setTimeout(function () {
                             instance.dpDiv.css({
                                 top: top-190, //you can adjust this value accordingly
                                 left: left + textBoxWidth//show at the end of textBox
                         });
                     }, 0);

             },
            timePicker: true,
            locale: {format: 'DD/MM/YYYY hh:mm A'},
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-secondary',
            cancelClass: 'btn-primary',
            startDate: moment().subtract(1, 'days'),
            endDate: moment()
        },function (start, end) {
            $('.input-daterange-timepicker').html(start.format('DD/MM/YYYY hh:mm A') + ' - ' + end.format('DD/MM/YYYY hh:mm A'));
            console.log(start.toISOString(), end.toISOString());
            //$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            var qs = "?container_id=" + id + "&t1='" + start.toISOString() + "'&t2='" + end.toISOString() + "'"
            Http.open("GET", url+qs);
            Http.send();

        },function ($) {
            "use strict";
            $.ChartJs.init()
        }(window.jQuery));

    },
        $.FormPickers = new FormPickers, $.FormPickers.Constructor = FormPickers

}(window.jQuery),

    //initializing
function ($) {
    "use strict";
    $.FormPickers.init()
}(window.jQuery);
