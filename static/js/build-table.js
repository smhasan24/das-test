const Http = new XMLHttpRequest();
const url='https://yk7sz4yiee.execute-api.eu-west-2.amazonaws.com/dev/ui/containers';

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();

    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

function addRowHandlers(table) {
  var rows = table.getElementsByTagName("tr");
  for (i = 0; i < rows.length; i++) {
    var currentRow = table.rows[i];
    var createClickHandler = function(row) {
      return function() {
        var cell = row.getElementsByTagName("td")[0];
        var id = cell.innerHTML;
        //alert("id:" + id);
        location.href = "file:///Users/shasan/dev/dasa/UI/Minton-v6.1/Admin/dist/default/demo-selected-container.html?id=" + id;
      };
    };
    currentRow.onclick = createClickHandler(currentRow);
  }
}

Http.open("GET", url);
Http.send();

Http.onreadystatechange = (e) => {
  if(Http.readyState === XMLHttpRequest.DONE) {
    response = JSON.parse(Http.responseText);

    let table = document.getElementById("table-containers");
    let data = Object.keys(response[0]);
    generateTableHead(table, data);
    generateTable(table, response);
    addRowHandlers(table);
  }

}
