from Database.Postgres import Postgres

class AssestTracking:

    def __init__(self,assets):
        try :
            self.mac = assets['addr']
            self.timestamp = assets['timestamp']
        except KeyError as e:
            raise Exception(str(e) + " is missing from the payload : " + json.dumps(assets))
    def update_item_history(self):
        # use mac to check device list
        contianer = 1
        db = Postgres()
        sql = """SELECT device_id, item_id FROM device_item
        WHERE device_id = (
        SELECT device_id FROM devices WHERE UPPER(mac_address) = UPPER('{}')
        )
        """.format(self.mac)

        if len(db.get(sql)) == 0:
            return "Device not in database: " + self.mac
        device_id, item_id = db.get(sql)[0]

        # if item in item history
        sql = """select item_history_id, checked_in_out from item_container_history
        where container_id = {} and item_id = {}""".format(contianer, item_id)
        results = db.get(sql)
        if len(results)>0:
            id, checked_in = results[0]

            if checked_in:
                sql = """UPDATE item_container_history
                SET last_seen_ts = '{}'
                WHERE item_history_id = {}
                """.format(self.timestamp, id)
                db.put(sql)
                # update last seen
                return " updating an already checked in item"
            else:
                sql = """UPDATE item_container_history
                SET last_seen_ts = '{}',
                check_in_ts = '{}',
                checked_in_out = True
                WHERE item_history_id = {}
                """.format(self.timestamp, self.timestamp, id)
                db.put(sql)
                return "updating a returning item"
                # update checked_in_ts and last_seen_ts
        else:
            # insert row into item_container_history
            sql = """INSERT INTO item_container_history(checked_in_out, check_in_ts, item_id, container_id, last_seen_ts)
            VALUES (True, '{}', {}, {}, '{}')
            """.format(self.timestamp,item_id, contianer, self.timestamp)
            db.put(sql)
        return "item " + str(item_id) + " found"

    def item_removal_check(self):
        # item_container_history check every item in container 1 where last seen ts is > now - 10 mins
        db = Postgres()
        sql = """SELECT item_history_id, item_id FROM item_container_history
        WHERE last_seen_ts > current_timestamp - (10 ||' minutes')::interval
        AND container_id = {}
        """.format(container)
        results = db.get(sql)

        for result in results:
            id, item_id = result
            sql = """UPDATE item_container_history
            SET checked_in_out = false
            WHERE last_seen_ts < current_timestamp - (10 ||' minutes')::interval
            AND container_id = {}
            """.format(id)
            db.put(sql)
