from enum import Enum

class AccessTypes(Enum):
    DOOR_OPEN = 'door open'
    DOOR_CLOSE = 'door close'
    CARD_SWIPE = 'card swipe'
    CONTAINER_SAFE_ACCESS = 'container safe access'
    CONTAINER_ALARM = 'container alarm'
