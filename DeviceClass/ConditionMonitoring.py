import os
import json
import requests
from Database.Postgres import Postgres
from DeviceClass.AccessTypes import AccessTypes

class ConditionMonitoring:
    def __init__(self, payload):

        try :
            self.mac = payload['addr']
            self.timestamp = payload['timestamp']
            self.payload_raw = payload['payload']
        except KeyError as e:
            raise Exception(str(e) + " is missing from the payload : " + json.dumps(payload))

    def set_device_type(self):

        with open('DeviceClass/devices.txt') as file:

            devices = file.readlines()
            for device in devices:

                name = device.split(' ')[0]
                mac = device.split(' ')[1].strip('\n')

                if self.mac == mac:
                    self.device_type = name
                    return name


            raise Exception("Device Type unknown")

    def send(self):
        db = Postgres()
        if self.device_type == 'condition_monitoring':
            ts = self.timestamp
            # add variables from payload decoder

            sql = """INSERT INTO container_history(light, timestamp, container_id, gps_lat, gps_long, temperature, humidity, shock, altitude, battery_level)
            VALUES ({}, '{}', {}, {}, {}, {}, {}, {}, {}, {})
            """.format(300, ts, 1, 52.432150000, -2.123450000, 25, 55, 5, 145, 60)

            db.put(sql)
            return "yes"
        if self.device_type == 'item':
            sql = """SELECT device_id, item_id FROM device_item
            WHERE device_id = (
            SELECT device_id FROM devices WHERE mac_address = '{}'
            )
            """.format(self.mac)

            device_id, item_id = db.get(sql)[0]
            return "item " + str(item_id) + " found"
        if self.device_type == 'door':
            door_open = True
            last_state = get_last_state()
            if door_open:
                return "The door was open"

            if door_open:
                print('check last state')
                # if last state was none or safe access
                    # set container_access_state to door open
                # if last state was card swipped
                    # wait for door close to state that the container was accessed
                # if last state was door close with no card swipe
                    # raise alarm
            # if door closed
                # Check last state
                # If last state was door open after first entrance
                    # Container accessed, don't know who yet
                # If last state was one card swipe
                    # Person in container has not left
                # If last state was two card swipes
                    # Person has left contianer
                # If last state was door open
                    #
